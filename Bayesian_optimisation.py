#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Jan 27 13:52:59 2020

@author: Anthony Haman


File description : 
    
    This file contains a script base on the Optuna library. It includes functions to
    implement Bayesian optimisation.
    
    For graph analysis of the studies please refer to the study_analysis.ipynb file
    
    Links to the libraries documentation : 
        
        https://github.com/optuna/optuna
        
    Important : 
        
        For squlite databse viewing, please download DB browser for sqLite
    
    
Notes/Upgrades/To-do list : 
        
    - Validate use of the pruning method 
    (Wich one is better in wich case, see Optuna's documentation)
    
    - Determine how to use Optuna with PostgreSql database
    (The current implementation may cause error "Locked database" if
    too much parralel script run at the same time)
        
    
Simple example : 
    
    #####################################
    Import libraries
    #####################################
    
    import Optuna
    import XXX
    import XXX
    
    #####################################
    Define model to optimise
    #####################################
    
    def create_network(trial):
        
        # For fixed value 
        a = 3
        b = 2
        
        # For optimisation value 
        Notes :  - The last two values represent the optimisation range
                 - The trial.xxxx represent the kind of range search 
        
        c = trial.suggest_int('a', 1, 10)
        d = trial.suggest_loguniform('d', 1e-8, 1e+2)
        
        # Generate model
        model = function(a,b,c,d)
        
        return model 
        
    #####################################
    Define the objective function
    #####################################
    
    def objective(trial): 
        
        # Generate model
        model = create_network(trial)
        
        # Determine the trial error
        error = finderror(model)
        
        return error
        
    #####################################
    Run the script
    #####################################
    
    # Create study
    Notes :     - The direction can either be minimize or mazimize depending on wich direction wanted
                - The pruning offers the possibility to eliminate local minimum if encountered 
                    (IMPLIMENTATION NOT SHOWN IN EXAMPLE - PLEASE REFER TO OPTUNA DOCUMENTATION)
                - The study name can represent any desired name - It will registered in the created database
                - The storage represent the name of the database, only change the xxxxx value to desired name
                - The load_if_exists represent the ability to continue optimisation from a previous study
                    or create a new one (Please note that it will overwrite old study if set to false and
                    that study with same name already exists)
                    
    study = optuna.create_study(direction='minimize',
                            pruner=optuna.pruners.MedianPruner(),
                            study_name ='xxxxxxxxxxxxxxxxxxxxxx',
                            storage = 'sqlite:///xxxxxxxxxxxxxx.db',
                            load_if_exists=True)
    
    # Run the optimisation
    
    # For optimisation with time limit use (time in seconds) : 
    study.optimize(objective, timeout = 60000)
    
    # For optimisation with a fix number of test limit use : 
    study.optimize(objective, n_trials = 2000)
    
    Notes :     - It it possible to add the argument "n_jobs = XXX" after the timeout or n_trial
                    parameters. It offers a multiprocessing tool to run multiple script simultenously.
                    The number XXX represent the number of paralel jobs, for better performance replace 
                    XXX by the number of cores on your machine. 
    
"""

"""
==========================================================================
Import libraries
==========================================================================
"""

import optuna
import numpy as np
import numpy as np
from Dynamic import auv


 
"""
==========================================================================
Defining model to optimize
==========================================================================
"""

def create_network(trial):
    
    # Defining optimisation space
    eta = trial.suggest_uniform('eta', 0.001,5)
    lamda = trial.suggest_uniform('lamda', 0.001,5)
    phi = trial.suggest_uniform('phi', 0.001,5)
    kd = trial.suggest_uniform('kd', 0.001,500)
    a = trial.suggest_uniform('mEstimate', 0.001,10)
    b = trial.suggest_uniform('dEstimate', 0.001,10)

    
  
    
    # Position initiale et vitesse initiale
    Pos_0 = np.array([[0], [0], [0], [0], [0], [0]])
    Vit_0 = np.array([[0], [0], [0], [0], [0], [0]])

    model = auv(a,b,0,eta,lamda,phi,kd,P_0 = Pos_0, V_0 = Vit_0, control_type = 'Sliding', capteurs = None)
    
    return model



# Defining the objective function
def objective(trial):    

    # Creating reservoir computer and regression model
    model = create_network(trial)
    
    # Intialiser le positionnement et la vitesse désirée du sous-marin
    Pos_D = np.array([[20], [0], [0], [0], [0], [0]])

    # Résolution par la méthode d'Euler de la postion et de la vitesse du sous-marin
    model.solve(t_0=0, t_f=20, dt=500, Pos_D=Pos_D)
    error = model.error()

    return  error


if __name__ == '__main__':
    
    
    study = optuna.create_study(direction='minimize',
                            pruner=optuna.pruners.MedianPruner(),
                            study_name ='Optimisation_Sliding_10',
                            storage = 'sqlite:///optimisation_study.db',
                            load_if_exists=True)
    
    
    study.optimize(objective, n_trials = 100000)

    
    
    
   
     
