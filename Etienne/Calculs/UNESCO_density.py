from numpy import *
import matplotlib.pyplot as plt

def UNESCO_Pressure(S, T, P):

    a_0 = 999.842594
    a_1 = 6.793953 * pow(10, -2)
    a_2 = -9.095290 * pow(10, -3)
    a_3 = 1.001685 * pow(10, -4)
    a_4 = -1.120083 * pow(10, -6)
    a_5 = 6.536332 * pow(10, -9)

    b_0 = 8.2449 * pow(10, -1)
    b_1 = -4.0899 * pow(10, -3)
    b_2 = 7.6438 * pow(10, -5)
    b_3 = -8.2467 * pow(10, -7)
    b_4 = 5.3875 * pow(10, -9)

    c_0 = -5.7246 * pow(10, -3)
    c_1 = 1.0227 * pow(10, -4)
    c_2 = -1.6546 * pow(10, -6)
    d_0 = 4.8314 * pow(10, -4)

    rho_SMOW = a_0 + a_1 * T + a_2 * pow(T, 2) + a_3 * pow(T, 3) + a_4 * pow(T, 4) + a_5 * pow(T, 5)
    B_1 = b_0 + b_1 * T + b_2 * pow(T, 2) + b_3 * pow(T, 3) + b_4 * pow(T, 4)
    C_1 = c_0 + c_1 * T + c_2 * pow(T, 2)

    rho_0 = rho_SMOW + B_1 * S + C_1 * pow(S, 1.5) + d_0 * pow(S, 2)

    e_0 = 19652.210000
    e_1 = 148.420600
    e_2 = -2.327105
    e_3 = 1.360477 * pow(10, -2)
    e_4 = -5.155288 * pow(10, -5)

    f_0 = 54.674600
    f_1 = -0.603459
    f_2 = 1.099870 * pow(10, -2)
    f_3 = -6.167000 * pow(10, -5)

    g_0 = 7.9440 * pow(10, -2)
    g_1 = 1.6483 * pow(10, -2)
    g_2 = -5.3009 * pow(10, -4)

    K_w = e_0 + e_1 * T + e_2 * pow(T, 2) + e_3 * pow(T, 3) + e_4 * pow(T, 4)
    F_1 = f_0 + f_1 * T + f_2 * pow(T, 2) + f_3 * pow(T, 3)
    G_1 = g_0 + g_1 * T + g_2 * pow(T, 2)

    K_0 = K_w + F_1 * S + G_1 * pow(S, 1.5)

    h_0 = 3.23990
    h_1 = 1.43713 * pow(10, -3)
    h_2 = 1.16092 * pow(10, -4)
    h_3 = -5.77905 * pow(10, -7)

    i_0 = 2.28380 * pow(10, -3)
    i_1 = -1.09810 * pow(10, -5)
    i_2 = -1.60780 * pow(10, -6)

    j_0 = 1.91075 * pow(10, -4)

    k_0 = 8.50935 * pow(10, -5)
    k_1 = -6.12293 * pow(10, -6)
    k_2 = 5.27870 * pow(10, -8)

    m_0 = -9.9348 * pow(10, -7)
    m_1 = 2.0816 * pow(10, -8)
    m_2 = 9.1697 * pow(10, -10)

    A_w = h_0 + h_1 * T + h_2 * pow(T, 2) + h_3 * pow(T, 3)
    A_1 = A_w + (i_0 + i_1 * T + i_2 * pow(T, 2)) * S + j_0 * pow(S, 1.5)
    B_w = k_0 + k_1 * T + k_2 * pow(T, 2)
    B_2 = B_w + (m_0 + m_1 * T + m_2 * pow(T, 2)) * S

    K = K_0 + A_1 * P + B_2 * pow(P, 2)

    rho = rho_0 / (1 - P/K)

    return rho

nb_iterations = 100
salinity = 35
temp = 10
height = linspace(0, 300, nb_iterations)

density = empty(nb_iterations)
for i in range(nb_iterations):
    density[i] = UNESCO_Pressure(salinity, temp, height[i]/10)

plot1 = plt.plot(height, density)
plt.ticklabel_format(useOffset=False)
plt.xlabel('Profondeur [m]')
plt.ylabel('Densité [kg/m\N{SUPERSCRIPT THREE}]')
plt.title('Variation de la densité selon la profondeur')
plt.show()
