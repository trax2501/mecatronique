from numpy import *
import matplotlib.pyplot as plt

def VibrationHelice():
    lamda = 1.8751
    E = 68.9 * pow(10, 9)
    rho = 2700
    b = 0.0625
    h = 0.004
    L = 0.103
    V = b * h * L
    I = b * pow(h, 3) / 12

    f = pow(lamda, 2) * sqrt(E * I / (rho * V * pow(L, 3))) / (2 * pi)
    print(f)


def DensiteEau(salinity, temp, height):
    C = 999.83 + 5.053 * height - 0.048 * pow(height, 2)
    beta = 0.808 - 0.0085 * height
    alpha = 0.0708 * (1 + 0.351 * height + 0.068 * (1 - 0.0683 * height) * temp)
    gamma = 0.003 * (1 - 0.059 * height - 0.012 * (1 - 0.064 * height) * temp)
    rho = C + beta * salinity - alpha * temp - gamma * (35 - salinity) * temp

    return rho


nb_iterations = 100
salinity = 35
temp = 10
height = linspace(0, 20/1000, nb_iterations)

density = empty(nb_iterations)
for i in range(nb_iterations):
    density[i] = DensiteEau(salinity, temp, height[i])

plot1 = plt.plot(height * 1000, density)
plt.ticklabel_format(useOffset=False)
plt.xlabel('Profondeur [m]')
plt.ylabel('Densité [kg/m\N{SUPERSCRIPT THREE}]')
plt.title('Variation de la densité selon la profondeur')
plt.show()