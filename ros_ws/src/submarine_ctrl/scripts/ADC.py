#! /usr/bin/env python3

import rospy
import smbus

# addresses possibles :0x48, 0x49, 0x4a, 0x4b
ADC_address = 0x48

def read_ADC():
    # lit la valeur
    # formatte la valeur
    value = 0
    return value


if __name__ == "__main__":
    rospy.init_node('ADC', anonymous=True)
    rate = rospy.Rate(100)
    while not rospy.is_shutdown():
        #lire le capteur I2C
        read_ADC()
        rate.sleep()
