#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Jun  1 19:35:01 2020

@author: ubuntu
"""

import time

class PID (object) : 
    
    def __init__(self, kpPosition, kiPosition, kdPosition, kpVelocity, kiVelocity, kdVelocity, direction = 1):
         
        # Direction
        self.direction = direction
        
        # Gains
        self.kpPosition = kpPosition
        self.kiPosition = kiPosition
        self.kdPosition = kdPosition
        self.kpVelocity = kpVelocity
        self.kiVelocity = kiVelocity
        self.kdVelocity = kdVelocity
        
        # Errors
        self.errorPosition = 0
        self.errorVitesse = 0
        
        # Derivator
        self.derivatorPosition = 0
        self.derivatorVelocity = 0
        
        # Integrator
        self.integratorPosition = 0
        self.integratorVelocity = 0
        
        # General Output Value
        self.output = 0
        
        # General Position Output Value
        self.outputPosition = 0
        self.pOutputPosition = 0
        self.iOutputPosition = 0
        self.dOutputPosition = 0
        
        # General Velocity Output Value
        self.outputVelocity = 0
        self.pOutputVelocity = 0
        self.iOutputVelocity = 0
        self.dOutputVelocity = 0
        
        # Time
        self.lastUpdate = self.millis()
        self.updateTime = 100
        self.lastActualPosition = 0
        self.lastActualVitesse = 0
        
        # Anti wind-up
        self.lowerIntegralLimitPosition = -500
        self.upperIntegralLimitPosition = 500
        self.lowerIntegralLimitVitesse = -500
        self.upperIntegralLimitVitesse = 500
        
        # Output Position Limits
        self.lowerOuputLimitPosition = 0
        self.upperOutputLimitPosition = 0
        
        # Output Velocity Limits
        self.lowerOuputLimitVitesse = 0
        self.upperOutputLimitVitesse = 0

        
    def setIntegralLimits(self, lowerIntegralLimitPosition, upperIntegralLimitPosition, lowerIntegralLimitVitesse,
                          upperIntegralLimitVitesse):
        """
        Set the lower and upper limits for the integral output

        Parameters
        ----------
        :param lowerIntegralLimitPosition:
                Lower Integral Limit for the Position
        :param upperIntegralLimitPosition:
                Upper Integral Limit for the Position
        :param lowerIntegralLimitVitesse:
                Lower Integral Limit for the Velocity
        :param upperIntegralLimitVitesse:
                Upper Integral Limit for the Velocity
        """

        # Position limits
        self.lowerIntegralLimitPosition = lowerIntegralLimitPosition
        self.upperIntegralLimitPosition = upperIntegralLimitPosition
        # Velocity limits
        self.lowerIntegralLimitVitesse = lowerIntegralLimitVitesse
        self.upperIntegralLimitVitesse = upperIntegralLimitVitesse

    def setOutputLimits(self, lowerLimitPosition, upperLimitPosition, lowerLimitVitesse, upperLimitVitesse):
        """
        Set the lower and upper limits for the total output

        Parameters
        ----------
            :param upperLimitVitesse:
                    Upper Limit for the Velocity
            :param lowerLimitVitesse:
                    Lower Limit for the Velocity
            :param upperLimitPosition:
                    Upper Limit for the Position
            :param lowerLimitPosition:
                    Upper Limit for the Position

        """
        # Position Output Limits
        self.lowerOuputLimitPosition = lowerLimitPosition
        self.upperOutputLimitPosition = upperLimitPosition
        # Velocity Output Limits
        self.lowerOuputLimitVitesse = lowerLimitVitesse
        self.upperOutputLimitVitesse = upperLimitVitesse
        
    @staticmethod
    def millis():
        """
        Returns the current time in milliseconds

        Returns
        -------
        current time in milliseconds
        """

        return int(round(time.time() * 1000))   
    
    def compute(self, targetPosition, targetVitesse, targetAcceleration, actualPosition, actualVitesse):
        """
        Calculates the output based on the PID algorithm

        Parameters
        ----------
            :param targetAcceleration:
                    Desired Acceleration of the model
            :param actualVitesse:
                    Actual Velocity of the model
            :param actualPosition:
                    Actual Position of the model
            :param targetVitesse:
                    Desired Velocity of the model
            :param targetPosition:
                    Desired Position of the model
        """

        # now = self.millis(self)
        # timeDifference = now - self.lastUpdate

        # if timeDifference >= self.updateTime:

    # Position Error Value
        self.errorPosition = targetPosition - actualPosition

    # Proportional and Derivative Position Outputs
        self.pOutputPosition = self.errorPosition * self.kpPosition
        self.dOutputPosition = self.kdPosition * (self.errorPosition - self.derivatorPosition)

    # Derivator and Integrator Position Update
        self.derivatorPosition = self.errorPosition
        self.integratorPosition += self.errorPosition

    # Integral Position Ouput
        self.iOutputPosition = self.integratorPosition * self.kiPosition

    # Integral Position Ouput Saturation
        if self.iOutputPosition < self.lowerIntegralLimitPosition:
            self.iOutputPosition = self.lowerIntegralLimitPosition
        elif self.iOutputPosition > self.upperIntegralLimitPosition:
            self.iOutputPosition = self.upperIntegralLimitPosition
    # Overall Position Output
        self.outputPosition = self.pOutputPosition + self.iOutputPosition + self.dOutputPosition

    # Overall Position Output Saturation
        if self.outputPosition < self.lowerOuputLimitPosition:
            self.outputPosition = self.lowerOuputLimitPosition
        elif self.outputPosition > self.upperOutputLimitPosition:
            self.outputPosition = self.upperOutputLimitPosition

    # Velocity Error Value
        self.errorVitesse = targetVitesse - actualVitesse

    # Proportional and Derivative Velocity Value
        self.pOutputVelocity = self.errorVitesse * self.kpVelocity
        self.dOutputVelocity = self.kdVelocity * (self.errorVitesse - self.derivatorVelocity)

    # Derivator and Integor Velocity Update
        self.derivatorVelocity = self.errorVitesse
        self.integratorVelocity += self.errorVitesse

    # Integrator Velocity Value
        self.iOutputVelocity = self.integratorVelocity * self.kiVelocity

        if self.iOutputVelocity < self.lowerIntegralLimitVitesse:
            self.iOutputVelocity = self.lowerIntegralLimitVitesse
        elif self.iOutputVelocity > self.upperIntegralLimitVitesse:
            self.iOutputVelocity = self.upperIntegralLimitVitesse

    # Overall Velocity Output
        self.outputVelocity = self.pOutputVelocity + self.iOutputVelocity + self.dOutputVelocity

    # Overall Velocity Output Saturation
        if self.outputVelocity < self.lowerOuputLimitVitesse:
            self.outputVelocity = self.lowerOuputLimitVitesse
        elif self.outputVelocity > self.upperOutputLimitVitesse:
            self.outputVelocity = self.upperOutputLimitVitesse

    # Last Actual Position and Velocity Update
        self.lastActualPosition = actualPosition
        self.lastActualVitesse = actualVitesse

    # General Ouput Value
        self.output = targetAcceleration + self.outputPosition + self.outputVelocity

        return self.output


    def setControlLimits(self, lowerLimit, upperLimit, axis):
        if axis == "surge":
            index = 0
        elif axis == "sway":
            index = 1
        elif axis == "heave":
            index = 2
        elif axis == "roll":
            index = 3
        elif axis == "pitch":
            index = 4
        elif axis == "yaw":
            index = 5

        if self.Mat_T[index] > upperLimit:
            self.Mat_T[index] = upperLimit

        if self.Mat_T[index] < lowerLimit:
            self.Mat_T[index] = lowerLimit
            
            
  