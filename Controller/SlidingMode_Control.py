# -*- coding: utf-8 -*-
"""
Created on Thursday June 4th 2020 12:52

@author: stefceklic
"""
#################################################################
import numpy as np
from scipy.interpolate import interp1d
#################################################################

# class MultiStateSlidingModeControl(object):


class DecoupledSlidingModeControl(object):
    """
        Sliding Mode Control based on the Single-Input Single-Output (SISO) Affin System

        Hypothesis:
            - All kinematic cross-coupling terms are neglected.
            - All dynamic cross-coupling terms are neglected.

    """

    def __init__(self, mass_actual, mass_estimate, damping_actual, damping_estimate, restoringForces_actual,
                 restoringForces_estimate):

        # Dynamic System variables
        self.mass_actual = mass_actual
        self.mass_estimate = mass_estimate
        self.damping_actual = damping_actual
        self.damping_estimate = damping_estimate
        self.restoringForces_actual = restoringForces_actual
        self.restoringForces_estimate = restoringForces_estimate

        # Dynamic System errors
        self.massError = self.mass_estimate - self.mass_actual
        self.dampingError = self.damping_estimate - self.damping_actual
        self.restoringForcesError = self.restoringForces_estimate - self.restoringForces_actual

        # Control variables

        self.lamda = 0
        self.eta = 1
        self.phi = 1
        self.k = 0
        self.kd = 0
        self.T = 0

        # Sliding Surface variables initialization
        self.s = 0
        self.x_dot_error = 0
        self.x_error = 0

    def setControlVariables(self, lamda, eta, phi, kd):
        """
        Function :
            ======================================================================
            Defines control variables.
            ======================================================================

        Variables :
            ======================================================================
            lamda : Bandwidth
            eta   : Constant
            phi   : Boundary layer thickness
            kd    : Sliding mode Gain
            ======================================================================

        Equation :
            ======================================================================
            N/A
            ======================================================================
        """
        self.lamda = lamda
        self.eta = eta
        self.phi = phi
        self.kd = kd
        # self.k = k

    def getSlidingSurface(self, ActualPosition, ActualVelocity, DesiredPosition, DesiredVelocity):
        """
        Function :
            ======================================================================
            Gets the sliding surface.
            ======================================================================

        Variables :
            ======================================================================
            s            : Sliding surface
            xdot_error   : Difference between desired and state velocity
            lamda        : Bandwidth
            x_error      : Difference between desired and state position
            ======================================================================

        Equation :
            ======================================================================
            s = x_dot_error + lamda*x_error
            ======================================================================
        """

        self.x_dot_error = ActualVelocity - DesiredVelocity
        self.x_error = ActualPosition - DesiredPosition

        self.s = self.x_dot_error + self.lamda * self.x_error

        # return self.s

    def sgn(self):
        """
        Function :
            ========================================================================================
            Switching function for the sliding mode.
            ========================================================================================

        Variables :
            ========================================================================================
            s                    : Sliding surface
            ========================================================================================

        Equation :
            ========================================================================================
                       1     if s > 0                (Case 1)
             sgn(s) =  0     if s = 0                (Case 2)
                      -1     if s < 0                (Case 3)
            ========================================================================================
        """

        # Case 1 :
        if self.s > 0:
            return 1
        # Case 2 :
        elif self.s == 0:
            return 0
        # Case 3 :
        else:
            return -1

    def sat(self):
        """
            Function :
                ========================================================================================
                Assigns a low pass filter structure to the dynamics of the sliding surface s inside
                the boundary layer.
                ========================================================================================

            Variables :
                ========================================================================================
                s                    : Sliding surface
                phi                  : Boundary layer thickness
                ========================================================================================

            Equation :
                ========================================================================================
                    sat(s/phi) = sgn(s)      if   |s/phi| > 1      (Case 1)
                               = s/phi       ohterwise             (Case 2)
                ========================================================================================
        """

        # Case 1 :
        if abs(self.s / self.phi) > 1:
            saturation = self.sgn()
            # print("saturation")
            # print(saturation)
        # Case 2 :
        else:
            saturation = self.s/self.phi

        return saturation

    def getVirtualVelocityReference(self, ActualPosition, DesiredPosition, DesiredVelocity):
        """
            Function :
                ======================================================================
                Defines a virtual reference (velocity or acceleration).
                ======================================================================

            Variables :
                ======================================================================
                xdot_virtual : Virtual velocity/acceleration reference
                xdot_desired : Desired velocity/acceleration state
                lamda        : Bandwidth
                x_error      : Difference between desired and state position/velocity
                ======================================================================

            Equation :
                ======================================================================
                xdot_virtual = xdot_desired - lambda * x_error
                ======================================================================

        """

        virtualValue = DesiredVelocity - self.lamda * (ActualPosition - DesiredPosition)
        
        return virtualValue

    def K(self, actualVelocity, virtualVelocity, virtualAcceleration):
        """
            Function :
                ========================================================================================
                Defines the switching gain.
                ========================================================================================

            Variables :
                ========================================================================================
                k                    : Switching gain
                eta                  : Constant
                massError            : Difference between estimated and actual mass
                dampingError         : Difference between estimated and actual damping
                restoringForcesError : Difference between estimated and actual restoring forces
                xdot                 : Actual velocity
                xdot_r               : Virtual velocity reference
                xddot_r              : Virtual acceleration reference
                ========================================================================================

            Equation :
                ========================================================================================
                k = |massError * xddot_r + dampingError * |xdot|*|xdot_r| + restoringForcesError| + eta
                ========================================================================================
        """

        self.k = abs(self.massError * virtualAcceleration + self.dampingError * abs(actualVelocity) *
                     abs(virtualVelocity) + self.restoringForcesError) + self.eta

    def computeControlLaw(self, ActualPosition, DesiredPosition, ActualVelocity, DesiredVelocity, DesiredAcceleration):

        # Determine virtual Velocity and Acceleration
        virtualAcceleration = self.getVirtualVelocityReference(ActualVelocity, DesiredVelocity, DesiredAcceleration)
        virtualVelocity = self.getVirtualVelocityReference(ActualPosition, DesiredPosition, ActualVelocity)

        # Determine the sliding surface
        self.getSlidingSurface(ActualPosition, ActualVelocity, DesiredPosition, DesiredVelocity)

        # Determine K gain value
        self.K(ActualVelocity, virtualVelocity, virtualAcceleration)

        # Compute the Control Law value
        InertiaPart = self.massError * virtualAcceleration
        DissipativePart = self.dampingError * abs(ActualVelocity) * abs(virtualVelocity)

        kd_part = self.kd * self.s
        sat_part = self.k * self.sat()
        # self.T = self.massError * virtualAcceleration + self.dampingError * abs(ActualVelocity) * abs(virtualVelocity)
        # +self.restoringForcesError + self.kd * self.s - self.k * self.sat()
        self.T = InertiaPart + DissipativePart + self.restoringForcesError + kd_part + sat_part
        return self.T


"""
===========================================
DecoupledSlidingModeControl Tests
===========================================
"""
# m = 200  # kg
# d = 50   # kg/m
# mEstimate = 0.6 * m
# dEstimate = 1.5 * d
# restoringForces = 0
# rEstimate = 0
# Essai = DecoupledSlidingModeControl(m, mEstimate, d, dEstimate, restoringForces, rEstimate)
#
# eta = 0.1
# lamda = 1
# phi = 0.35
# kd = 200
# Essai.setControlVariables(lamda, eta, phi, kd)
# ActualPosition = 0
# DesiredPosition = 1
# ActualVelocity = 0
# DesiredVelocity = 1
# DesiredAcceleration = 1
# Essai.computeControlLaw(ActualPosition, DesiredPosition, ActualVelocity, DesiredVelocity, DesiredAcceleration)

"""
__init__ Validation --- WORKS
"""
# print("m_actual")
# print(Essai.mass_actual)
# print("m_estimate")
# print(Essai.mass_estimate)
# print("m_error")
# print(Essai.massError)
# print("d_actual")
# print(Essai.damping_actual)
# print("d_estimate")
# print(Essai.damping_estimate)
# print("d_error")
# print(Essai.dampingError)
# print("r_actual")
# print(Essai.restoringForces_actual)
# print("d_estimate")
# print(Essai.restoringForces_estimate)
# print("r_error")
# print(Essai.restoringForcesError)
"""
setControlVariables --- WORKS
"""
# print("lamda")
# print(Essai.lamda)
# print("phi")
# print(Essai.phi)
# print("eta")
# print(Essai.eta)
# print("kd")
# print(Essai.kd)
"""
computeControlLaw --- WORKS
"""
# print("Control T")
# print(Essai.T)
