


#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu May 28 12:09:07 2020

@author: ubuntu
"""

import numpy as np
from Dynamic import auv

"""
==========================================================
Hypothèses 
==========================================================

1) Les forces liées au lift peuvent être négligées étant données des vitesses d'opération relativement faibles

2) Le sous-marin est approximativement symétrique selon les 3 axes

3) Le pitch et le roll sont controllés passivement et donc considéré négligeable

4) Le centre de gravité est placé aux coordonnées 0,0,0.

5) Le sous-marin est toujours considéré à l'horizontal

6) Les degrés de liberté sont découplés, ainsi la force de coriolis et centrifuge peuvent être négligé

7) Le sway est négligé

8) L'impact des courrants marin et des perturbations sont négligés

"""


"""
==========================================================
Résolution
==========================================================
"""

# Position initiale et vitesse initiale
Pos_0 = np.array([[0], [0], [0], [0], [0], [0]])
Vit_0 = np.array([[0], [0], [0], [0], [0], [0]])

# Création du modèle
solOcean = auv( P_0 = Pos_0, V_0 = Vit_0, control_type = 'PID', capteurs = 'All')

# Intialiser le positionnement et la vitesse désirée du sous-marin
# Pos_D = np.array([[10], [0], [0], [0], [0], [0]])
# Vit_D = np.array([[0], [0], [0], [0], [0], [0]])
# Acc_D = np.array([[0], [0], [0], [0], [0], [0]])

# Résolution par la méthode d'Euler de la postion et de la vitesse du sous-marin
solOcean.solve(t_0=0, t_f=20, dt=500)#, Pos_D=Pos_D)
error = solOcean.error()

# # Affichage de la position actuelle et désirée, et de la vitesse actuelle et désirée du sous-marin
solOcean.plot(axe='All')




