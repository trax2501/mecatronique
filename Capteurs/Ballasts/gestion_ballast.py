"""
fichier         :   gestion_ballast.py
auteur          :   Étienne St-Pierre
date            :   26-05-2020
modifications
    Nom         :
    Date        :
    Description :

Description du script:
Traitement des données des ballasts.
Le script gère la traduction des mesures capteurs de pression en info utile et des commandes de position en valeur
mesurable par les capteurs.
"""

from numpy import *
from scipy.interpolate import interp1d
import matplotlib.pyplot as plt
from scipy.optimize import *

class GestionBallast:

    def __init__(self, _diametre, _epaisseur):
        self.dia = _diametre  # [m]
        self.ep = _epaisseur  # [m]
    """
    Description :   Transforme une mesure de pression en volume d'eau dans un ballast
    Entrées     :   Valeur de pression
    Pré         :   Valeur < Pression ballast plein
                    Valeur >= 0
    Post        :   Aucun
    """
    def pression_a_volume(self, _pression):
        _hauteur = _pression / (RHO_EAU * G_TERRE)
        # Mesure du volume d'eau
        if _hauteur <= self.dia / 2:
            _theta = 2 * arccos(1 - 2 * _hauteur / self.dia)
            _aire_tri = (pow(self.dia, 2) - 2 * _hauteur * self.dia) * sin(_theta / 2) / 4
            _aire_arc = _theta * pow(self.dia, 2) / 8
            _aire_seg = _aire_arc - _aire_tri
            _volume_actuel = _aire_seg * self.ep * 1000

        elif _hauteur <= self.dia:
            _hauteur = self.dia - _hauteur
            _theta = 2 * arccos(1 - 2 * _hauteur / self.dia)
            _aire_tri = (pow(self.dia, 2) - 2 * _hauteur * self.dia) * sin(_theta / 2) / 4
            _aire_arc = _theta * pow(self.dia, 2) / 8
            _aire_seg = (pi * pow(self.dia, 2) / 4) - (_aire_arc - _aire_tri)
            _volume_actuel = _aire_seg * self.ep * 1000
        else:
            print('error')
            _volume_actuel = None

        return _volume_actuel

    """
    Description :   Prediction de l'acceleration du sous-marin selon le volume combine des ballasts
    Entrées     :   Volume combine des ballasts
    Pré         :   Volume < Volume 2 ballasts pleins
                    Valeur >= 0
    Post        :   Aucun
    """
    def acceleration(self, volume_actuel):
        a_z = (RHO_EAU * VOLUME_SM / (MASSE_SM + RHO_EAU * volume_actuel/1000) - 1) * G_TERRE
        print(a_z)
        return a_z

    """
        Description :   Transforme une demande en acceleration a une commande de volume combine des ballasts
        Entrées     :   Acceleration voulue
        Pré         :   Acc. < Acc. max
                        Acc. >= Acc. min
        Post        :   Aucun
    """
    def commande_volume(self, acc_z):
        V_ballast = (VOLUME_SM / (acc_z / G_TERRE + 1) - MASSE_SM / RHO_EAU) * 1000
        print(V_ballast)
        return V_ballast

    """
        Description :   Transforme une mesure de volume en pression mesuree dans un ballast
        Entrées     :   Volume
        Pré         :   Volume < Volume max ballast
                        Volume >= 0
        Post        :   Aucun
    """
    def volume_a_pression(self, volume_a_convertir):
        _nb_points = 100
        _pression = linspace(0, self.dia * RHO_EAU * G_TERRE, _nb_points)
        _volume = empty(_nb_points)
        for i in range(_nb_points):
            _volume[i] = self.pression_a_volume(_pression[i])

        # plt.plot(_volume, _pression)
        # plt.title('Mesure de pression attendue selon volume d''eau dans un ballast')
        # plt.xlabel('Volume d''eau dans un ballast [L]')
        # plt.ylabel('Pression attendue [m/s2]')
        # plt.show()
        interp_pression = interp1d(_volume, _pression)

        _pression_correspondante = interp_pression(volume_a_convertir)

        return _pression_correspondante


RHO_EAU = 1023.6  # [kg/m^3]
G_TERRE = 9.81  # [N/kg]
VOLUME_SM = 0.5  # [m^3]
MASSE_SM = 473  # [Kg]
ballast1 = GestionBallast(0.5, 0.122)
volume = linspace(0, 51, 100)
acc_z = empty(100)
for i in range(100):
    acc_z[i] = ballast1.acceleration(volume[i])
plt.plot(volume, acc_z)
plt.title('Acceleration selon volume total d''eau dans les ballasts')
plt.xlabel('Volume total d''eau dans les ballasts [L]')
plt.ylabel('Acceleration du sous-marin [m/s2]')
plt.show()
vol = ballast1.commande_volume(0)
pression = ballast1.volume_a_pression(15)
print('Pression:')
print(pression)
volume = ballast1.pression_a_volume(4835)
acc = ballast1.acceleration(37.9 + 0.15)
print('acc')
print(acc)
volume_1 = ballast1.commande_volume(0)
print('vol')
print(volume_1)

# Estimation du temps de déplacement
# v_0 = 0
# delta_x = 15
# time = (-2 * v_0 + sqrt(pow(2 * v_0, 2) - 4 * 2 * -delta_x * acc_z)) / (2 * acc_z)
# if time < 0:
#     time = (-2 * v_0 - sqrt(pow(2 * v_0, 2) - 4 * 2 * -delta_x * acc_z)) / (2 * acc_z)
# print(time)

# v_z = v_0 + acc_z * time
# print(v_z)
