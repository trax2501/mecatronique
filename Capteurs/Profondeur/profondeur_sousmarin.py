def pression_a_profondeur(_pression):
    _profondeur = _pression / (RHO_EAU * G_TERRE)

    return _profondeur


def profondeur_a_pression(_profondeur):
    _pression = _profondeur * RHO_EAU * G_TERRE

    return _pression


RHO_EAU = 1027  # [kg/m3]
G_TERRE = 9.81  # [N/kg]
