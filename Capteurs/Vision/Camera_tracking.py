#!/usr/bin/env python

"""
Created on Thu May 28 12:09:07 2020

@author: ubuntu


Notes : 
    
    - Add camera calibrating function 
    - Add running average for camera video distance 
    
    
"""

"""
==========================================================
Importer les librairies
==========================================================
"""
import cv2
import os
import argparse
import math
import numpy as np

# local libraries
from .diverDetector import DiverDetection
from .utils import draw_boxes_and_labels, check_file_ext

"""
==========================================================
Classe du tracking par caméra
==========================================================
"""

class Vision_tracking: 
    
    def __init__(self):
        
        self.im_dir = '/home/ubuntu/Documents/Université/Session E20/IMC916 - Projet de conception II/mecatronique/Capteurs/Vision/test_data/'
        self.IMAGE_PATHS = [os.path.join(self.im_dir, f) for f in os.listdir(self.im_dir) if check_file_ext(f)]
        
        self.focal_length   =  30 # [mm] Focal length of camera

        self.human_height = 1828.8 # [mm]
        self.human_width  = 609.6 # [mm]
        self.human_depth  = 609.6 # [mm]
        
        
    def detect(self):
        
        self.obj_classes = {1: 'Diver', 2: 'ROV'}
        self.drDetect = DiverDetection()
        
        for im_file in self.IMAGE_PATHS:
            #print ("Testing {0}".format(im_file))
            frame = cv2.imread(im_file)
            localized_objs = self.drDetect.Detect_multi_objs(frame) 
            
            self.estimate_posture(debug = False)
            x, z, theta = self.estimate_distance()
         
            if len(localized_objs)>0:
                frame = draw_boxes_and_labels(frame, localized_objs, self.obj_classes, self.posture, self.accuracy)
        
            cv2.imshow("Annotated Output", frame)
            cv2.waitKey(5000) 
            cv2.destroyAllWindows()
        
        return x/1000, z/1000, theta
    
         
    def estimate_posture (self, debug = False) :
        
        # Define shape ratio
        self.box_width  =  self.drDetect.x_f - self.drDetect.x_0
        self.box_height =  self.drDetect.y_f - self.drDetect.y_0
        
        post_ratio = self.box_height / self.box_width
        
        error = 0.35
        
        old_posture = str('Estimating')
        
        up_ratio = self.human_height / self.human_width
        x_ratio  = 1/ (self.human_height / self.human_width)
        y_ratio  = self.human_depth / self.human_width
        
        if post_ratio > up_ratio - 3*error : 
            self.posture = str('Going up/down')
            if post_ratio < 3 : 
                self.accuracy = 1 - abs(up_ratio-post_ratio)/(3*error)
            else : 
                self.accuracy = 1
            old_posture = self.posture
        elif post_ratio < x_ratio + error and post_ratio > x_ratio - error : 
            self.posture = str('Going sideways')
            self.accuracy = 1 - abs(x_ratio-post_ratio)/(2*error)
            old_posture = self.posture
        elif post_ratio < y_ratio + error and post_ratio > y_ratio - error : 
            self.posture = str('Going straight')
            self.accuracy = 1 - abs(y_ratio-post_ratio)/(2*error)
            old_posture = self.posture
        else :
            self.posture = old_posture
            self.accuracy = 0
            
             
        if debug is True : 

            print('Height', self.drDetect.height )
            print('Width', self.drDetect.width )
            print('X_0 : ', self.drDetect.x_0, 'X_f : ',  self.drDetect.x_f )
            print('Y_0 : ', self.drDetect.y_0, 'Y_f : ',  self.drDetect.y_f )
            print('Center - X : ',self.drDetect.width/2,' Y : ',self.drDetect.height/2 )
            print('Ratio',post_ratio)
            print('up_ratio range :', up_ratio +error ,' to ', up_ratio - 3*error )
            print('side_ratio range :', x_ratio +error,' to ', x_ratio  -error)
            print('straight_ratio range :', y_ratio +error,' to ', y_ratio -error)
            print(self.posture)
       
    def estimate_distance (self):
        
        old_real_height = self.human_depth
        old_real_width  = self.human_height
        
        if self.posture == 'Going up/down' : 
            real_height = self.human_height
            real_width  = self.human_width
            
            old_real_height = real_height
            old_real_width  = real_width
            
        elif self.posture == 'Going sideways':
            real_height = self.human_width
            real_width  = self.human_height
            
            old_real_height = real_height
            old_real_width  = real_width
            
        elif self.posture == 'Going straight': 
            real_height = self.human_depth
            real_width  = self.human_height
            
            old_real_height = real_height
            old_real_width  = real_width
            
        else : 
            real_height = old_real_height
            real_width  = old_real_width
        
        distance1 = self.focal_length * real_height /  self.box_height
        distance2 = self.focal_length * real_width  / self.box_width
        
        distance = (distance1 + distance2)/2
        
        x, z,theta = self.estimate_relative_position(distance)

        return x, z, theta
    
    
    def estimate_relative_position(self, distance) : 
        
        # Define middle coordonate of diver
        x_m = (self.drDetect.x_f + self.drDetect.x_0)/2 
        y_m = (self.drDetect.y_f + self.drDetect.y_0)/2 
        
        # Define coordonate relative to center of camera
        x = -self.drDetect.width/2  + x_m
        y = -self.drDetect.height/2 + y_m
        
        old_partial_height = (y / self.box_height)  * self.human_depth
        old_partial_width  = (x /  self.box_width)  * self.human_height
        
        if self.posture == 'Going up/down' : 
            partial_height = (y / self.box_height)  * self.human_height
            partial_width  = (x /  self.box_width)  * self.human_width
            
            old_partial_height = partial_height
            old_partial_width  = partial_width
            
        elif self.posture == 'Going sideways':
            partial_height = (y / self.box_height)  * self.human_width
            partial_width  = (x /  self.box_width)  * self.human_height
            
            old_partial_height = partial_height
            old_partial_width  = partial_width
            
        elif self.posture == 'Going straight': 
            partial_height = (y / self.box_height)  * self.human_depth
            partial_width  = (x /  self.box_width)  * self.human_height
            
            old_partial_height = partial_height
            old_partial_width  = partial_width
        
        else : 
            partial_height =  old_partial_height 
            partial_width  =  old_partial_width
        
        
        # Define angle to align diver
        theta = np.arctan(partial_width/distance)
        
        # Define up/down distance 
        z = partial_height
        
        # Define horizontal distance 
        x = distance
        
        # print('partial w : ', partial_width)
        # print('Partial height',partial_height)
        # print('Distance : ',distance,'(mm)')
        # print('Horzontal distance :', x,'(mm)')
        # print('Angle : ',theta,'deg and z : ',z,'(mm)')
    
        return x, z, theta 
       
if __name__ == "__main__":
    
    a = Vision_tracking()
    x, z, theta = a.detect()








