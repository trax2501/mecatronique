#!/usr/share/env python
"""
------------------------------------------------------------------------------------------------------------------------
Description: classe d'un système dynamique
Par: Xavier Trahan
Date: 16/02/20
------------------------------------------------------------------------------------------------------------------------
"""
from vecteurs import Vecteur

class SysDyn:
    masse = 0
    position = Vecteur(0, 0)
    vitesse = Vecteur(0, 0)
    accel = Vecteur(0, 0)

    def set_accel(self, Force):
        self.accel.set_x(Force.get_x() / self.masse)
        self.accel.set_y(Force.get_y() / self.masse)
        return

    def integrate(self, dt,):
        self.position = self.position + self.vitesse * dt
        self.vitesse = self.position + self.accel * dt
        return

    def get_position(self):
        return self.position

    def get_vitesse(self):
        return self.vitesse

    def get_accel(self):
        return self.accel

if __name__ == "__main__":
    print("Système dynamique")
