#!/usr/share/env python
"""
------------------------------------------------------------------------------------------------------------------------
Description: classe de vecteur
Par: Xavier Trahan
Date: 16/02/20
------------------------------------------------------------------------------------------------------------------------
"""
import numpy.linalg as LA
import numpy as np

class Vecteur:

    _magnitude = 0
    _composante_x = 0
    _composante_y = 0
    _argument = 0

    def __init__(self, x, y):
        self._composante_x = x
        self._composante_y = y
        self._magnitude = self.calcul_mag()
        self._argument = self.calc_argument()

    def __add__(self, other):
        x = self._composante_x + other.get_x()
        y = self._composante_y + other.get_y()
        return Vecteur(x, y)

    def __sub__(self, other):
        x = self._composante_x - other.get_x()
        y = self._composante_y - other.get_y()
        return Vecteur(x, y)

    def __mul__(self, other):
        if type(other) == float or type(other) == int:
            x = self._composante_x * other
            y = self._composante_y * other
        else:
            x = self._composante_x
            y = self._composante_y
        return Vecteur(x, y)

    def __neg__(self):
        return Vecteur(-self._composante_x, -self._composante_y)

    def __str__(self):
        output = "x : " + str(self._composante_x) + "\n" + "y : " + str(self._composante_y)
        return output

    def calcul_mag(self):
        return np.sqrt(self._composante_x ** 2 + self._composante_y ** 2)

    def set_x(self, x):
        self._composante_x = x
        self._magnitude = self.calcul_mag()
        self._argument = self.calc_argument()
        return

    def set_y(self, y):
        self._composante_y = y
        self._magnitude = self.calcul_mag()
        self._argument = self.calc_argument()
        return

    def get_x(self):
        return self._composante_x

    def get_y(self):
        return self._composante_y

    def get_arg(self):
        return self._argument

    def calc_argument(self):
        return np.arctan2(self._composante_y, self._composante_x)

if __name__ == "__main__":
    Force1 = Vecteur(2, 2)
    Force2 = Vecteur(1, 2)

    print(Force1 + -Force2)
    print(Force2)