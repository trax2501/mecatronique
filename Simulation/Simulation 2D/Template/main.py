#!/usr/share/env python
"""
------------------------------------------------------------------------------------------------------------------------
Description: Boucle principale de la simulation 
Par: Xavier Trahan
Date: 16/02/20
------------------------------------------------------------------------------------------------------------------------
"""
import matplotlib.pyplot as plt
import numpy as np
from sous_marin import SousMarin
from vecteurs import Vecteur

dt = 0.01           #sec
t_tot = 10          #sec
t = np. arange(0, t_tot, dt)
p0 = Vecteur(0, 0)  #position initiale
v0 = Vecteur(1, 1)  #vitesse_initiale
masse = 10          #kg

# definition du logging
log = [[]]

SM = SousMarin(masse, p0, v0)

somme_forces = Vecteur(1, 1) * 10

# definition des forces


for i in range(len(t)):
    # update force

    # reaction du système
    SM.set_accel(somme_forces)
    SM.integrate(dt)

    # log resultats
    log[0].append(SM.get_position().get_x())

plt.plot(t, log[0])
plt.show()







