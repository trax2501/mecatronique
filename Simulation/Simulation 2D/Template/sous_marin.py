#!/usr/share/env python
"""
------------------------------------------------------------------------------------------------------------------------
Description: classe du sous-marin
Par: Xavier Trahan
Date: 16/02/20
------------------------------------------------------------------------------------------------------------------------
"""
import system_dynamique as SD

class SousMarin(SD.SysDyn):

    def __init__(self, masse, position, vitesse):
        self.masse = masse
        self.position = position
        self.vitesse = vitesse
        return
