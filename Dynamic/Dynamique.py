#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu May 28 12:09:07 2020

@author: ubuntu
"""
import time
import matplotlib.pyplot as plt
from scipy.integrate import odeint, RK45
from numpy.linalg import inv
import numpy as np
import math
from Controller.PID_control import PID
from Controller.SlidingMode_Control import DecoupledSlidingModeControl
from Capteurs.Vision.Camera_tracking import Vision_tracking

"""
==========================================================
Hypothèses 
==========================================================

1) Les forces liées au lift peuvent être négligées étant données des vitesses d'opération relativement faibles

2) Le sous-marin est approximativement symétrique selon les 3 axes

3) Le pitch et le roll sont controllés passivement et donc considéré négligeable

4) Le centre de gravité est placé aux coordonnées 0,0,0.

5) Le sous-marin est toujours considéré à l'horizontal

6) Les degrés de liberté sont découplés, ainsi la force de coriolis et centrifuge peuvent être négligé

7) Le sway est négligé

8) L'impact des courrants marin et des perturbations sont négligés

"""

"""
==========================================================
Classe simple sous-marin 
==========================================================
"""


class auv:

    """
    NOTES À FAIRE:
    - MATRICE L POUR LE POSITIONNEMENT DES MOTEURS
    - POSITIONNEMENT DES BALLASTS
    - MODÉLISATION DES BALLASTS
    - VITESSE DESIRÉE
    - ACCEL DÉSIRÉE
    - TRAJECTOIRE SÉQUENTIELLE (EX: VA A 5m, PUIS A 3m)
    - INTERFACE QUI AFFICHE LIVE SPEC (PROFONDEUR, RESERVE D'AIR)
    - AJOUTER FONCTION POUR MODIFIER LA POSITION INITIALE
    - AJOUTER FONCTION POUR GÉRER UNE TRAJECTOIRE
    - AJOUTER THRESHOLD FORCE MOTEUR
    
    """

    """ A class used to represent a dynamic model of an Autonomous Underwater Vehicle
    # Attributes
    ------------
    None
    ------------

    Methods
    -------
    set IntegralLimits(lowerLimit, upperLimit)
        Limits the integral windup to the limits

    """

    def __init__(self, P_0 = None, V_0 = None, control_type = None, capteurs = 'all'):
       
        """
        Description : 
            
            Initialisation of the l'objet AUV

        Parameters
        ----------
        P_0 : TYPE, optional
            DESCRIPTION. The default is None.
            
        V_0 : TYPE, optional
            DESCRIPTION. The default is None.
            
        control : TYPE, optional
            DESCRIPTION. The default is None.

        Returns
        -------
        None.

        """
        # Définir type de control
        self.control_type = control_type

        # Paramètres de l'environnement
        self.g   = 9.81          # [m/s²] est la constante gravitationnelle
        self.rho = 998           # [kg/m³]est la densité du fluide

        # Paramètres du modèle
        self.m = 502             # [kg] est la masse du sous-marin

        self.V_model = 0.531     # [m3] est le volume de fluide déplacé par le sous-marin
        self.V_enceinte = 0.394  # [m3] est le volume de fluide dans l'enceinte de chargement
        self.V_ballast = 0.0255  # [m3] est le volume de interne d'une ballast

        self.Ixx = 809           # [kgm²] est l'inertie selon l'axe x
        self.Iyy = 818           # [kgm²] est l'inertie selon l'axe y
        self.Izz = 264           # [kgm²] est l'inertie selon l'axe z

        self.L  = 1.00           # [m] est la longueur totale du sous-marin
        self.l1 = 0.00           # [m] est la distance du moteur 1 jusqu au centre de masse
        self.l2 = 0.00           # [m] est la distance du moteur 2 jusqu au centre de masse
        self.l3 = 0, 6858        # [m] est la distance de la ballaste 1 jusqu au centre de masse
        self.l4 = 0, 6858        # [m] est la distance de la ballaste 2 jusqu au centre de masse

        self.rG = [0, 0, 0]      # est le centre de gravité du sous-marin
        self.rB = [0, 0, 0.1]    # est le centre de flotaison du sous-marin relatif au centre de gravité

        # Coefficients de drag linéaire
        self.Xu = 120            # [Ns/m] est le paramètre de drag linéaire selon le surge
        self.Yv = 90             # [Ns/m] est le paramètre de drag linéaire selon le sway
        self.Zw = 150            # [Ns/m] est le paramètre de drag linéaire selon le heave
        self.Kp = 0              # [Ns/m] est le paramètre de drag linéaire selon le roll
        self.Mq = 0              # [Ns/m] est le paramètre de drag linéaire selon le pitch
        self.Nr = 18             # [Ns/m] est le paramètre de drag linéaire selon le yaw

        # Coefficients de drag quadratique
        self.Xuu = 90            # [Ns²/m²] est le paramètre de drag quadratique selon le surge
        self.Yvv = 90            # [Ns²/m²] est le paramètre de drag quadratique selon le sway
        self.Zww = 120           # [Ns²/m²] est le paramètre de drag quadratique selon le heave
        self.Kpp = 0             # [Ns²/m²] est le paramètre de drag quadratique selon le roll
        self.Mqq = 0             # [Ns²/m²] est le paramètre de drag quadratique selon le pitch
        self.Nrr = 15            # [Ns²/m²] est le paramètre de drag quadratique selon le yaw

        # Définir position intiale selon le plan B 
        if P_0 is None:
            self.X_B = np.array([[0], [0], [0]])
            self.R_B = np.array([[0], [0], [0]])
        else:
            self.X_B = np.array([P_0[0], P_0[1], P_0[2]])
            self.R_B = np.array([P_0[3], P_0[4], P_0[5]])

        self.P_B = np.concatenate((self.X_B, self.R_B), axis=0)

        # Définir vitesse initiale selon le plan B 
        if V_0 is None:
            self.v_B = np.array([[0], [0], [0]])
            self.w_B = np.array([[0], [0], [0]])
        else:
            self.v_B = np.array([V_0[0], V_0[1], V_0[2]])
            self.w_B = np.array([V_0[3], V_0[4], V_0[5]])

        self.V_B = np.concatenate((self.v_B, self.w_B), axis=0)
        
        # Initialiser matrice de masse et d'inertie
        self.Mat_M = self.M()

        # Initialiser matrice de drag
        self.Mat_D = np.zeros((6, 6))

        # Initialiser matrice de flotabilité et gravitationnelle
        self.Mat_G = np.zeros((6,1))
        
        # Initialiser matrice de torque
        self.Mat_T = np.zeros((6,1))
        self.Mat_T0 = np.zeros((6,1))
        # self.Mat_T0 = ([0],[0],[0],[0],[0],[0])
        
        # Definition parametre de flotabilité et gravitationnel
        self.W = self.m   * self.g
        self.B = self.rho * self.g * self.V_model
        
        
        # Initialisation des capteurs
        self.capteurs = capteurs
        if self.capteurs == 'All' :
            camera = Vision_tracking()
            x,z,theta = camera.detect()
            self.Pos_D = np.array([[x], [0], [z], [0], [0], [theta]])
            self.Vit_D = np.array([[0], [0], [0], [0], [0], [0]])
            self.Acc_D = np.array([[0], [0], [0], [0], [0], [0]])
        else : 
            # Initialiser vecteur de position, vitesse et accélération désirée
            self.Pos_D = np.array([[0], [0], [0], [0], [0], [0]])
            self.Vit_D = np.array([[0], [0], [0], [0], [0], [0]])
            self.Acc_D = np.array([[0], [0], [0], [0], [0], [0]])
                
             
    def M(self):

        # Definir l'elipse équivalente pour approximer les coefficients de masse ajoutés
        self.d = ((6 * self.V_model) / (math.pi * self.rho * self.L))

        # Déterminer les coefficients de masse ajoutée selon les axe x,y et z
        kx = -0.00047 * (self.L / self.d) ** 2 + 0.0134 * (self.L / self.d) - 0.059
        ky = -0.00088 * (self.L / self.d) ** 2 + 0.0245 * (self.L / self.d) + 0.805
        kz = ky

        # Déterminer le moment d'inertie équivalent pour l'élipsoide
        Iyy = (math.pi * self.rho / 30) * (self.L * self.d) ** 4 * ((self.L / self.d) ** 2 + 1)

        # Déterminer la masse et le momment d'inertie non dimensionalisé
        m_nd = self.V_model / ((1 / 2) * self.rho * self.L ** 3)
        Iyy_nd = Iyy / ((1 / 2) * self.rho * self.L ** 5)
        Izz_nd = Iyy_nd

        # Déterminer les valeurs de masse ajoutée
        self.Xdu = 0
        self.Ydv = -ky * m_nd
        self.Zdw = -kz * m_nd
        self.Kdp = 0
        self.Mdq = -ky * Iyy_nd
        self.Ndr = -kz * Izz_nd

        # Définir la matrice de masse ajoutée
        MA = np.array([[self.Xdu, 0, 0, 0, 0, 0],
                       [0, self.Ydv, 0, 0, 0, 0],
                       [0, 0, self.Zdw, 0, 0, 0],
                       [0, 0, 0, self.Kdp, 0, 0],
                       [0, 0, 0, 0, self.Mdq, 0],
                       [0, 0, 0, 0, 0, self.Ndr]])

        # Définir la matrice de masse
        MRB = np.array([[self.m, 0, 0, 0, 0, 0],
                        [0, self.m, 0, 0, 0, 0],
                        [0, 0, self.m, 0, 0, 0],
                        [0, 0, 0, self.Ixx, 0, 0],
                        [0, 0, 0, 0, self.Iyy, 0],
                        [0, 0, 0, 0, 0, self.Iyy]])

        return (MA + MRB)

    def D(self, n):

        # Définir la matrice DL
        DL = np.array([[self.Xu, 0, 0, 0, 0, 0],
                       [0, self.Yv, 0, 0, 0, 0],
                       [0, 0, self.Zw, 0, 0, 0],
                       [0, 0, 0, self.Kp, 0, 0],
                       [0, 0, 0, 0, self.Mq, 0],
                       [0, 0, 0, 0, 0, self.Nr]])

        # Définir la matrice DQ
        DQ = np.array([[self.Xuu * abs(np.float64(n[6])), 0, 0, 0, 0, 0],
                       [0, self.Yvv * abs(np.float64(n[7])), 0, 0, 0, 0],
                       [0, 0, self.Zww * abs(np.float64(n[8])), 0, 0, 0],
                       [0, 0, 0, self.Kpp * abs(np.float64(n[9])), 0, 0],
                       [0, 0, 0, 0, self.Mqq * abs(np.float64(n[10])), 0],
                       [0, 0, 0, 0, 0, self.Nrr * abs(np.float64(n[11]))]])

        return (DL + DQ)

    def G(self, n):
        
        # Définir les coefficients de la matrice G
        G1 =  (self.W - self.B) * math.sin(n[4])
        G2 = -(self.W - self.B) * math.cos(n[4]) * math.sin(n[3])
        G3 = -(self.W - self.B) * math.cos(n[4]) * math.cos(n[3])
        G4 = -(self.rG[1] * self.W - self.rB[1] * self.B) * math.cos(n[4]) * math.cos(n[3]) + (self.rG[2] * self.W - self.rB[2] * self.B) * math.cos(n[4]) * math.sin(n[3])
        G5 =  (self.rG[2] * self.W - self.rB[2] * self.B) * math.sin(n[4]) + (self.rG[0] * self.W - self.rB[0] * self.B) * math.cos(n[4]) * math.cos(n[3])
        G6 = -(self.rG[0] * self.W - self.rB[0] * self.B) * math.cos(n[4]) * math.sin(n[3]) - (self.rG[1] * self.W - self.rB[1] * self.B) * math.sin(n[4])

        # Définir la matrice G
        Mat_G = np.array([[G1],[G2],[G3],[G4],[G5],[G6]])

        return Mat_G
    
    def control(self, n, DOF = 'all'):
        
        if self.control_type == 'PID' :
            
            if DOF == 'all':
                Control_surge = PID(kpPosition = 150, kiPosition = 50, kdPosition = 0, kpVelocity = 100, kiVelocity = 100, kdVelocity = 20)
                Control_sway  = PID(kpPosition = 0  , kiPosition = 0 , kdPosition = 0, kpVelocity = 0  , kiVelocity = 0  , kdVelocity = 0 )
                Control_heave = PID(kpPosition = 50 , kiPosition = 20, kdPosition = 1, kpVelocity = 100, kiVelocity = 100, kdVelocity = 20)
                Control_roll  = PID(kpPosition = 0  , kiPosition = 0 , kdPosition = 0, kpVelocity = 0  , kiVelocity = 0  , kdVelocity = 0 )
                Control_pitch = PID(kpPosition = 0  , kiPosition = 0 , kdPosition = 0, kpVelocity = 0  , kiVelocity = 0  , kdVelocity = 0 )
                Control_yaw   = PID(kpPosition = 50 , kiPosition = 20, kdPosition = 1, kpVelocity = 100, kiVelocity = 100, kdVelocity = 20)
            
            else : 
                
                if 'surge' in DOF :
                    Control_surge = PID(kpPosition =150, kiPosition = 50, kdPosition = 0, kpVelocity = 100, kiVelocity = 100, kdVelocity = 20)
                else : 
                    Control_surge = None
                
                if 'sway' in DOF :
                    Control_sway = PID(kpPosition = 0 , kiPosition = 0 , kdPosition = 0, kpVelocity = 0  , kiVelocity = 0  , kdVelocity = 0 )
                else : 
                    Control_sway = None
                    
                if 'heave' in DOF :
                    Control_heave = PID(kpPosition = 50, kiPosition = 20, kdPosition = 1, kpVelocity = 100, kiVelocity = 100, kdVelocity = 20)
                else : 
                    Control_heave = None
                    
                if 'roll' in DOF :
                    Control_roll = PID(kpPosition = 0 , kiPosition = 0 , kdPosition = 0, kpVelocity = 0  , kiVelocity = 0  , kdVelocity = 0 )
                else : 
                    Control_roll = None
                    
                if 'pitch' in DOF :
                    Control_pitch = PID(kpPosition = 0 , kiPosition = 0 , kdPosition = 0, kpVelocity = 0  , kiVelocity = 0  , kdVelocity = 0 )
                else : 
                    Control_pitch = None
                    
                if 'yaw' in DOF :
                    Control_yaw = PID(kpPosition = 50, kiPosition = 20, kdPosition = 1, kpVelocity = 100, kiVelocity = 100, kdVelocity = 20)
                else : 
                    Control_yaw = None
                    
            self.Mat_Control = ([Control_surge,
                                 Control_sway,
                                 Control_heave,
                                 Control_roll,
                                 Control_pitch,
                                 Control_yaw])
            
            for DOF in range(len(self.Mat_Control)):
                if self.Mat_Control[DOF] is not None :
                    self.Mat_Control[DOF].setOutputLimits(-1000, 1000, -1000, 1000)
                    self.Mat_Control[DOF].setIntegralLimits(-1000, 1000, -1000, 1000)
                    self.Mat_T0[DOF] = self.Mat_Control[DOF].compute(self.Pos_D[DOF], self.Vit_D[DOF], self.Acc_D[DOF], np.float64(n[DOF]),  np.float64(n[DOF+6]))
  
        elif self.control_type == 'Sliding':
            
            if DOF == "surge":
                index = 0
                m = self.Mat_M[index, 0]  # kg
                d = self.Mat_D[index, 0]  # kg/m
                mEstimate = 0.8 * m
                dEstimate = 1.5 * d
                restoringForces = self.Mat_G[index, 0]
                rEstimate = 0 * restoringForces
                
                
                mEstimate = mEstimate * m
                dEstimate = dEstimate * d
                restoringForces = self.Mat_G[index, 0]
                rEstimate = rEstimate * restoringForces
                
                ControlSurgeSliding = DecoupledSlidingModeControl(m, mEstimate, d, dEstimate, restoringForces, rEstimate)

                ControlSurgeSliding.setControlVariables(lamda = 0.5, eta = 2.2, phi = 0.8, kd= 750)
                pos_d = np.float64(self.Pos_D[index])

                vit_d = np.float64(self.Vit_D[index])

                acc_d = np.float64(self.Acc_D[index])

                pos_a = np.float64(n[index])

                vit_a = np.float64(n[index+6])

                Sliding = ControlSurgeSliding.computeControlLaw(pos_d, vit_d, acc_d, pos_a, vit_a)

                # print("sliding")
                # print(Sliding)

                self.Mat_T0 = ([Sliding],
                               [0],
                               [0],
                               [0],
                               [0],
                               [0])
               
    def setControlLimits(self, lowerLimit, upperLimit, axis):
        if axis == "surge":
            index = 0
        elif axis == "sway":
            index = 1
        elif axis == "heave":
            index = 2
        elif axis == "roll":
            index = 3
        elif axis == "pitch":
            index = 4
        elif axis == "yaw":
            index = 5

        if self.Mat_T[index] > upperLimit:
            self.Mat_T[index] = upperLimit

        if self.Mat_T[index] < lowerLimit:
            self.Mat_T[index] = lowerLimit

  
    def model(self, n, t):

        # Definir la matrice D
        self.Mat_D = self.D(n)

        # Definir la matrice G
        self.Mat_G = self.G(n)
 
        # Definir la matrice de torque
        if self.control_type is not None :
            self.control(n, DOF = 'surge, heave, yaw')
            #self.control(n, DOF = 'surge')
    
        # Definir le vecteur vitesse
        self.vitesse = np.array(n[6:]).reshape((6,1))
        
        # Génération de la matrice de controle T
        if self.control_type == "PID":
            self.Mat_T = (np.dot(self.Mat_M, self.Mat_T0)) + (np.dot(self.Mat_D, self.vitesse)) + self.Mat_G
        elif self.control_type == "Sliding":
            self.Mat_T = self.Mat_T0

     	# Set limits on the control law values T
        self.setControlLimits(-710, 710, "surge")
        self.setControlLimits(-489, 489, "heave")
        self.setControlLimits(-487, 487, "yaw")

        # Matrice des forces totales résultantes
        self.Mat_F = self.Mat_T - (np.dot(self.Mat_D, self.vitesse)) - self.Mat_G
       
        # Definir matrice d'accelerations
        self.A_B = inv(self.Mat_M) @ self.Mat_F

        return ([np.float64(n[6]), np.float64(n[7]), np.float64(n[8]), np.float64(n[9]), np.float64(n[10]),
                 np.float64(n[11]), np.float64(self.A_B[0]), np.float64(self.A_B[1]), np.float64(self.A_B[2]),
                 np.float64(self.A_B[3]), np.float64(self.A_B[4]), np.float64(self.A_B[5])])

    def solve(self, t_0=0, t_f=30, dt=200, Pos_D = None, Vit_D = None, Acc_D = None):

        if Pos_D is not None :
            self.Pos_D = Pos_D
        if Vit_D is not None :
            self.Vit_D =Vit_D 
        if Acc_D is not None : 
            self.Acc_D = Acc_D
            
        # Définition du vecteur de position et vitesse initiale
        n_0 = np.array(
            [np.float64(self.P_B[0]), np.float64(self.P_B[1]), np.float64(self.P_B[2]), np.float64(self.P_B[3]),
             np.float64(self.P_B[4]), np.float64(self.P_B[5]), np.float64(self.V_B[0]), np.float64(self.V_B[1]),
             np.float64(self.V_B[2]), np.float64(self.V_B[3]), np.float64(self.V_B[4]), np.float64(self.V_B[5])])

        t, dt_ = np.linspace(t_0, t_f, dt, retstep=True)
        self.t = t
    
        sol = odeint(self.model, n_0, t, args=())

        # Definition de la position
        self.x = sol[:,0]  # Position selon le surge
        self.y = sol[:,1]  # Position selon le sway
        self.z = sol[:,2]  # Position selon le heave
        self.φ = sol[:,3]  # Position selon le roll
        self.θ = sol[:,4]  # Position selon le pitch
        self.ψ = sol[:,5]  # Position selon le yaw
        
        # Definition de la vitesse
        self.u = sol[:,6]  # Vitesse selon le surge
        self.v = sol[:,7]  # Vitesse selon le sway
        self.w = sol[:,8]  # Vitesse selon le heave
        self.p = sol[:,9]  # Vitesse selon le roll
        self.q = sol[:,10] # Vitesse selon le pitch
        self.r = sol[:,11] # Vitesse selon le yaw      
        
        # Definition de l'acceleration
        dt_new = (t_f - t_0)/dt
        
        self.acc_x = np.zeros((dt,))
        self.acc_y = np.zeros((dt,))
        self.acc_z = np.zeros((dt,))
        self.acc_φ = np.zeros((dt,))
        self.acc_θ = np.zeros((dt,))
        self.acc_ψ = np.zeros((dt,))    
        
        for id_ in range(len(self.u)):
            if id_ == 0:
                self.acc_x[id_] = 0
            else :
                dv = self.u[id_] - self.u[id_-1]
                self.acc_x[id_] = dv/dt_new

        for id_ in range(len(self.v)):
            if id_ == 0:
                self.acc_y[id_] = 0
            else :  
                dv = self.v[id_] - self.v[id_-1]
                self.acc_y[id_] = dv/dt_new
 
                
        for id_ in range(len(self.w)):
            if id_ == 0:
                self.acc_z[id_] = 0
            else : 
                dv = self.w[id_] - self.w[id_-1]
                self.acc_z[id_] = dv/dt_new
                
        for id_ in range(len(self.p)):
            if id_ == 0:
                self.acc_φ[id_] = 0
            else : 
                dv = self.p[id_] - self.p[id_-1]
                self.acc_φ[id_] = dv/dt_new

        for id_ in range(len(self.q)):
            if id_ == 0:
                self.acc_θ[id_] = 0
            else : 
                dv = self.q[id_] - self.q[id_-1]
                self.acc_θ[id_] = dv/dt_new

        for id_ in range(len(self.r)):
            if id_ == 0:
                self.acc_ψ[id_] = 0
            else :  
                dv = self.r[id_] - self.r[id_-1]
                self.acc_ψ[id_] = dv/dt_new
          

    def error(self):
        
        goal = self.Pos_D[0]
        
        total_err = 0 
        
        for id_ in range(len(self.x)):
            
            error = abs( goal - self.x[id_])
            total_err += error
            
        return total_err
            
    def plot(self, axe=None):

        if axe == 'All':
            p1 = self.x # Position selon le surge
            v1 = self.u # Vitesse selon le surge
            pd1 = self.Pos_D[0]
            a1 = self.acc_x
            
            p2 = self.y # Position selon le sway
            v2 = self.v # Vitesse selon le sway
            pd2 = self.Pos_D[1]
            a2 =  self.acc_y 
            
            p3 = self.z # Position selon le heave
            v3 = self.w # Vitesse selon le heave
            pd3 = self.Pos_D[2]
            a3 = self.acc_z
            
            p4= self.φ # Position selon le roll
            v4 = self.p # Vitesse selon le roll
            pd4 = self.Pos_D[3]
            a4 = self.acc_φ
            
            p5 = self.θ # Position selon le pitch
            v5 = self.q # Vitesse selon le pitch
            pd5 = self.Pos_D[4]
            a5 = self.acc_θ
            
            p6 = self.ψ # Position selon le yaw
            v6 = self.r # Vitesse selon le yaw
            pd6 = self.Pos_D[5]
            a6 = self.acc_ψ
            
            fig, axs = plt.subplots(2, 3)
            
            axs[0, 0].plot(self.t, p1, label='Position du sous-marin')
            if self.control_type is not None : 
                axs[0, 0].plot([0,self.t[-1]],[pd1,pd1], label='Position desirée du sous-marin')
            axs[0, 0].plot(self.t, v1, label='Vitesse du sous-marin')
            axs[0, 0].plot(self.t, a1  ,'--'    , label='Acceleration du sous-marin')
            axs[0, 0].set_title('Surge')
            
            axs[0, 1].plot(self.t, p2, label='Position du sous-marin')
            if self.control_type is not None : 
                axs[0, 1].plot([0,self.t[-1]],[pd2,pd2], label='Position desirée du sous-marin')
            axs[0, 1].plot(self.t, v2, label='Vitesse du sous-marin')
            axs[0, 1].plot(self.t, a2 ,'--'  , label='Acceleration du sous-marin')
            axs[0, 1].set_title('Sway')
            
            axs[0, 2].plot(self.t, p3, label='Position du sous-marin')
            if self.control_type is not None : 
                axs[0, 2].plot([0,self.t[-1]],[pd3,pd3], label='Position desirée du sous-marin')
            axs[0, 2].plot(self.t, v3, label='Vitesse du sous-marin')
            axs[0, 2].plot(self.t, a3   ,'--'   , label='Acceleration du sous-marin')
            axs[0, 2].set_title('Heave')
            
            axs[1, 0].plot(self.t, p4, label='Position du sous-marin')
            if self.control_type is not None : 
                axs[1, 0].plot([0,self.t[-1]],[pd4,pd4], label='Position desirée du sous-marin')
            axs[1, 0].plot(self.t, v4, label='Vitesse du sous-marin')
            axs[1, 0].plot(self.t, a4  ,'--'    , label='Acceleration du sous-marin')
            axs[1, 0].set(xlabel='Time [s]')
            axs[1, 0].set_title('Roll')
            
            axs[1, 1].plot(self.t, p5, label='Position du sous-marin')
            if self.control_type is not None : 
                axs[1, 1].plot([0,self.t[-1]],[pd5,pd5], label='Position desirée du sous-marin')
            axs[1, 1].plot(self.t, v5, label='Vitesse du sous-marin')
            axs[1, 1].plot(self.t, a5  ,'--'    , label='Acceleration du sous-marin')
            axs[1, 1].set(xlabel='Time [s]')
            axs[1, 1].set_title('Pitch')
            
            axs[1, 2].plot(self.t, p6, label='Position du sous-marin')
            if self.control_type is not None : 
                axs[1, 2].plot([0,self.t[-1]],[pd6,pd6], label='Position desirée du sous-marin')
            axs[1, 2].plot(self.t, v6, label='Vitesse du sous-marin')
            axs[1, 2].plot(self.t, a6   ,'--'   , label='Acceleration du sous-marin')
            axs[1, 2].set(xlabel='Time [s]')
            if self.control_type is not None : 
                axs[1, 2].legend(bbox_to_anchor=(0.85,0), loc="lower right", bbox_transform=fig.transFigure, ncol=4)
            else : 
                axs[1, 2].legend(bbox_to_anchor=(0.85,0), loc="lower right", bbox_transform=fig.transFigure, ncol=3)
            axs[1, 2].set_title('Yaw')
            
        else : 
        
            if axe == 'surge' :
                position = self.x # Position selon le surge
                vitesse = self.u # Vitesse selon le surge
                position_desire = self.Pos_D[0]
                acc = self.acc_x
            elif axe == 'sway':
                position = self.y # Position selon le sway
                vitesse = self.v # Vitesse selon le sway
                position_desire = self.Pos_D[1]
                acc =  self.acc_y 
            elif axe == 'heave':
                position = self.z # Position selon le heave
                vitesse = self.w # Vitesse selon le heave
                position_desire = self.Pos_D[2]
                acc = self.acc_z
            elif axe == 'roll':
                position = self.φ # Position selon le roll
                vitesse = self.p # Vitesse selon le roll
                position_desire = self.Pos_D[3]
                acc = self.acc_φ
            elif axe == 'pitch':
                position = self.θ # Position selon le pitch
                vitesse = self.q # Vitesse selon le pitch
                position_desire = self.Pos_D[4]
                acc = self.acc_θ
            elif axe == 'yaw' :
                position = self.ψ # Position selon le yaw
                vitesse = self.r # Vitesse selon le yaw
                position_desire = self.Pos_D[5]
                acc = self.acc_ψ
            else : 
                print ('SVP choisir un axe à afficher')
            
            if self.control_type is not None : 
                plt.figure()
                plt.plot(self.t, position, label='Position du sous-marin')
                plt.plot([0,self.t[-1]],[position_desire,position_desire], label='Position desirée du sous-marin')
                plt.plot(self.t, vitesse, label='Vitesse du sous-marin')
                plt.plot(self.t, acc    , label='Acceleration du sous-marin')
                plt.title('Position et vitesse selon le ' + axe)
                plt.xlabel('Time [s]')
                plt.legend()
                plt.show()
                # print('position_desire',position_desire)
                # print('vit_desire',vitesse_desire)
            else : 
                plt.figure()
                plt.plot(self.t, position, label='Position du sous-marin')
                plt.plot(self.t, vitesse, label='Vitesse du sous-marin')
                plt.title('Position et vitesse selon le ' + axe)
                plt.xlabel('Time [s]')
                plt.legend()
                plt.show()




